package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.example.demo.model.Employee;
import com.example.demo.repository.EmployeeRepository;


@Service
public class EmployeeService {
	
	private final EmployeeRepository employeeRepository;
	
	@Autowired
	public EmployeeService(EmployeeRepository employeeRepository) {
		super();
		this.employeeRepository = employeeRepository;
	}
	
	public Employee addEmployee(Employee employee) {
		employee.setStatus(true);
		return employeeRepository.save(employee);
	}
	
	public List<Employee> getAllEmploye(){
		return employeeRepository.findAll();
	}
	
	public Employee updateEmployee(Employee employee) {
		return employeeRepository.save(employee);
	}
	
	public Employee findEmployeeById(Long id) {
		Optional<Employee> find = employeeRepository.findById(id);
		if(find.isPresent()) {
			return find.get();
		}
		return null;
	}
	
	public void deleteEmployee(Long id) {
		employeeRepository.delete(findEmployeeById(id));
	}
	
	public String testAvail() {
		return "new1";
	}
	
//	@Scheduled(cron = "* * 8 * * *", zone = "GMT+7")
//	private void testSchedule() {
//		System.out.println("schedule jalan");
//	}
}
